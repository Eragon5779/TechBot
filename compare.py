from terminaltables import AsciiTable
from discord import Embed

def compare(cpu1, cpu2, cpu1_name, cpu2_name) :
    len_counter = 0
    if "Intel" in cpu1_name :
        cpu1_name = cpu1_name.replace("Processor ","").split("Intel®")[1].split("(")[0]
    if "Intel" in cpu2_name :
        cpu2_name = cpu2_name.replace("Processor ","").split("Intel®")[1].split("(")[0]
    embed = Embed(title="Comparison", color=0xffff00)
    embed.add_field(name="CPU", value="", inline=True)
    embed.add_field(name=cpu1_name, value="", inline=True)
    embed.add_field(name=cpu2_name, value="", inline=True)
    embed.add_field(name="Cores", value="", inline=True)
    embed.add_field(name=cpu1.cores, value="", inline=True)
    embed.add_field(name=cpu2.cores, value="", inline=True)
    embed.add_field(name="Threads", value="", inline=True)
    embed.add_field(name=cpu1.threads, value="", inline=True)
    embed.add_field(name=cpu2.threads, value="", inline=True)
    embed.add_field(name="Base Clock", value="", inline=True)
    embed.add_field(name=cpu1.base_clock, value="", inline=True)
    embed.add_field(name=cpu2.base_clock, value="", inline=True)
    embed.add_field(name="Boost Clock", value="", inline=True)
    embed.add_field(name=cpu1.boost_clock, value="", inline=True)
    embed.add_field(name=cpu2.boost_clock, value="", inline=True)
    embed.add_field(name="Cache", value="", inline=True)
    embed.add_field(name=cpu1.cache, value="", inline=True)
    embed.add_field(name=cpu2.cache, value="", inline=True)
    embed.add_field(name="TDP", value="", inline=True)
    embed.add_field(name=cpu1.tdp, value="", inline=True)
    embed.add_field(name=cpu2.tdp, value="", inline=True)
    embed.add_field(name="Release Date", value="", inline=True)
    embed.add_field(name=cpu1.release_date, value="", inline=True)
    embed.add_field(name=cpu2.release_date, value="", inline=True)
    headers = ['Spec',cpu1_name, cpu2_name]
    cores = ['Cores', cpu1.cores, cpu2.cores]
    threads = ['Threads', cpu1.threads, cpu2.threads]
    base_clock = ['Base Clock', cpu1.base_clock, cpu2.base_clock]
    boost_clock = ['Boost Clock', cpu1.boost_clock, cpu2.boost_clock]
    cache = ['Cache', cpu1.cache, cpu2.cache]
    tdp = ['TDP', cpu1.tdp, cpu2.tdp]
    release_date = ['Release Date', cpu1.release_date, cpu2.release_date]
    table_data = [
        headers,
        cores,
        threads,
        base_clock,
        boost_clock,
        cache,
        tdp,
        release_date
    ]
    table = AsciiTable(table_data)
    #print(embed)
    return table
    #return embed
