# TechBot v1.0.1

## What is TechBot?

TechBot is a Discord bot that helps people with their technology needs

## Commmands
* $intel
  * Searches the Intel Ark for a specified processor
* $amd
  * Searches SpecDB for a specified processor
* $compare
  * Compares 2 processors' specs
 
## Features

* Automatically pulls from SpecDB and Intel ARK daily
* Updates daily to keep current information

## To-Do
* Tag person in response
* Multi-term searches
* Cross-DB searches

## Thanks

* markasoftware for help with the SpecDB!