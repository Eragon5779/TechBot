# These are the dependecies. The bot depends on these to function, hence the name. Please do not change these unless your adding to them, because they can break the bot.
import discord,  random, asyncio
from discord.ext.commands import Bot
from discord.ext import commands
import platform, requests, urllib3, json
from terminaltables import AsciiTable
import update
from datetime import datetime, timedelta
from time import sleep
import time
import compare
from queue import Queue
from threading import Thread

client = Bot(description="TechBot by eragon5779#1448", command_prefix="$", pm_help = False)

amd = json.loads(open('specs.json').read())
intel = json.loads(open('intel.json').read())
intel_cpus = {}
amd_cpus = {}
runTime = "00:01"

class intel_cpu :
    def __init__(self, cores, threads, base_clock, boost_clock, cache, tdp, release_date, max_memory, lithography, socket, multiple_cpu) :
        self.cores = cores
        self.threads = threads
        self.base_clock = base_clock
        self.boost_clock = boost_clock
        self.cache = cache
        self.tdp = tdp
        self.release_date = release_date
        self.max_memory = max_memory
        self.lithography = lithography
        self.socket = socket
        self.multiple_cpu = multiple_cpu

class amd_cpu :
    def __init__(self, cores, threads, base_clock, boost_clock, cache, tdp, release_date, lithography, socket) :
        self.cores = cores
        self.threads = threads
        self.base_clock = base_clock
        self.boost_clock = boost_clock
        self.cache = cache
        self.tdp = tdp
        self.release_date = release_date
        self.lithography = lithography
        self.socket = socket

def amd_search(search, amd_cpus) :
    matching = []
    cpuList = ""
    count = 0
    for cpu in amd_cpus.keys() :
        if search.lower() in cpu.lower():
            matching.append(cpu)
    matching.sort()
    for cpu in matching :
        cpuList += '[%d] %s\n' % (count, cpu)
        count += 1

    return matching, cpuList, count

def intel_search(search, intel_cpus) :
    matching = []
    cpuList = ""
    count = 0
    for cpu in intel_cpus.keys() :
        if search.lower() in cpu.lower() :
            matching.append(cpu)
    matching.sort()
    for cpu in matching :
        cpuList += '[%d] %s\n' % (count, cpu)
        count += 1
    return matching, cpuList, count

def update_files(intel, amd, intel_cpus, amd_cpus) :
    intel, amd = update.update()
    load_data(intel_cpus, amd_cpus, intel, amd)
    print('Data updated at {}'.format(datetime.now()))

@client.event
async def on_ready():
        print('https://discordapp.com/oauth2/authorize?client_id={}&scope=bot&permissions=121920'.format(client.user.id))
        return await client.change_presence(game=discord.Game(name='$help Providing Tech Help')) #This is buggy, let us know if it doesn't work.


@client.command(pass_context=True, name="intel")
async def _intel(ctx, *, arg="") :
    search = ''
    if len(ctx.message.content) > 6 :
        # search = ctx.message.content.split(' ')[1]
        search = arg
    else :
        await client.send_message(ctx.message.channel, 'Please enter model you want to search for (i.e. 6700)')

        msg = await client.wait_for_message(author=ctx.message.author)
        search = msg.content

    matching, cpuList, count = intel_search(search, intel_cpus)

    if len(cpuList) < 1 :
        await client.send_message(ctx.message.channel, "No processors found")
        return
    try :
        await client.send_message(ctx.message.channel, cpuList)
    except :
        await client.send_message(ctx.message.channel, 'Error: List too large. Please refine search.')
        return
    choice = 0
    if count > 1 :
        await client.send_message(ctx.message.channel, 'Which CPU do you want?')
        msg = await client.wait_for_message(author=ctx.message.author)
        try :
            choice = int(msg.content)
            if choice > count or choice < 0 :
                raise ValueError('Out of Range')
        except :
            await client.send_message(ctx.message.channel, "Error: Not a number or Out of Range. Please restart your search.")
            return
    wantedData = {
        'name':'Name',
        'cores':'Cores',
        'threads':'Threads',
        'base_clock':'Base Clock',
        'boost_clock':'Boost Clock',
        'cache':'Cache',
        'tdp':'TDP',
        'release_date':'Release Date',
        'max_memory':'Max Memory',
        'lithography':'Lithography',
        'socket':'Socket',
        'multiple_cpu':'Multiple CPU'
    }
    cpu_data = vars(intel_cpus[matching[choice]])
    userOut = "```"
    cpu_name = matching[choice].replace("Processor ","").split("Intel®")[1].split("(")[0]
    embed = discord.Embed(title=cpu_name, color=0x0000ff)
    for need in wantedData.keys() :
        spaces = " " * (17 - len(wantedData[need]))
        if need == 'name' :
            userOut += spaces + ('{}: {}\n'.format(wantedData[need], matching[choice]))
        else :
            userOut += spaces + ('{}: {}\n'.format(wantedData[need], cpu_data[need]))
            embed.add_field(name=wantedData[need], value=cpu_data[need], inline=True)


    userOut += '```'
    #await client.send_message(ctx.message.channel, userOut)
    await client.send_message(ctx.message.channel, embed=embed)

@client.command(pass_context=True, name="amd")
async def _amd(ctx, *, arg="") :
    search = ''
    if len(ctx.message.content) > 6 :
        #search = ctx.message.content.split(' ')[1]
        search = arg
    else :
        await client.send_message(ctx.message.channel, 'Please enter model you want to search for (i.e. 1700)')

        msg = await client.wait_for_message(author=ctx.message.author)
        search = msg.content

    matching, cpuList, count = amd_search(search, amd_cpus)

    if len(cpuList) < 1 :
        await client.send_message(ctx.message.channel, "No processors found")
        return
    try :
        await client.send_message(ctx.message.channel, cpuList)
    except :
        await client.send_message(ctx.message.channel, 'Error: List too large. Please refine search.')
        return
    choice = 0
    if count > 1 :
        await client.send_message(ctx.message.channel, 'Which CPU do you want?')
        msg = await client.wait_for_message(author=ctx.message.author)
        try :
            choice = int(msg.content)
            if choice > count or choice < 0 :
                raise ValueError('Out of Range')
        except :
            await client.send_message(ctx.message.channel, "Error: Not a number or Out of Range. Please restart your search.")
            return
    wantedData = {
        'name':'Name',
        'cores':'Cores',
        'threads':'Threads',
        'base_clock':'Base Clock',
        'boost_clock':'Boost Clock',
        'cache':'Cache',
        'tdp':'TDP',
        'release_date':'Release Date',
        'lithography':'Lithography',
        'socket':'Socket'
    }
    cpu_data = vars(amd_cpus[matching[choice]])
    userOut = "```"
    embed = discord.Embed(title=matching[choice], color=0xff0000)
    for need in wantedData.keys() :
        spaces = " " * (17 - len(wantedData[need]))
        if need == 'name' :
            userOut += spaces + ('{}: {}\n'.format(wantedData[need], matching[choice]))
        else :
            userOut += spaces + ('{}: {}\n'.format(wantedData[need], cpu_data[need]))
            embed.add_field(name=wantedData[need], value=cpu_data[need], inline=True)

    userOut += '```'
    #embed.add_field()
    #await client.send_message(ctx.message.channel, userOut)
    await client.send_message(ctx.message.channel, embed=embed)

@client.command(pass_context=True, name="compare")
async def _compare(ctx) :
    await client.send_message(ctx.message.channel, "Which brand would you like for the first CPU? (Intel or AMD)")

    cpu1 = ''
    cpu2 = ''
    cpu1_name = ''
    cpu2_name = ''
    msg = await client.wait_for_message(author=ctx.message.author)

    if msg.content.lower() == 'amd' :
        await client.send_message(ctx.message.channel, "Please enter search term (i.e. 2700): ")

        msg = await client.wait_for_message(author=ctx.message.author)
        search = msg.content
        matching, cpuList, count = amd_search(search, amd_cpus)

        if len(cpuList) < 1 :
            await client.send_message(ctx.message.channel, "No processors found")
            return
        try :
            await client.send_message(ctx.message.channel, cpuList)
        except :
            await client.send_message(ctx.message.channel, 'Error: List too large. Please refine search.')
            return
        choice = 0
        if count > 1 :
            await client.send_message(ctx.message.channel, 'Which CPU do you want?')
            msg = await client.wait_for_message(author=ctx.message.author)
            try :
                choice = int(msg.content)
                if choice > count or choice < 0 :
                    raise ValueError('Out of Range')
            except :
                await client.send_message(ctx.message.channel, "Error: Not a number or Out of Range. Please restart your search.")
                return

        cpu1 = amd_cpus[matching[choice]]
        cpu1_name = matching[choice]
    elif msg.content.lower() == 'intel' :
        await client.send_message(ctx.message.channel, "Please enter search term (i.e. 8700): ")

        msg = await client.wait_for_message(author=ctx.message.author)
        search = msg.content
        matching, cpuList, count = intel_search(search, intel_cpus)

        if len(cpuList) < 1 :
            await client.send_message(ctx.message.channel, "No processors found")
            return
        try :
            await client.send_message(ctx.message.channel, cpuList)
        except :
            await client.send_message(ctx.message.channel, 'Error: List too large. Please refine search.')
            return
        choice = 0
        if count > 1 :
            await client.send_message(ctx.message.channel, 'Which CPU do you want?')
            msg = await client.wait_for_message(author=ctx.message.author)
            try :
                choice = int(msg.content)
                if choice > count or choice < 0 :
                    raise ValueError('Out of Range')
            except :
                await client.send_message(ctx.message.channel, "Error: Not a number or Out of Range. Please restart your search.")
                return

        cpu1 = intel_cpus[matching[choice]]
        cpu1_name = matching[choice].split('(')[0]

    else :
        await client.send_message(ctx.message.channel, "Unknown brand. Stopping search")
        return
    await client.send_message(ctx.message.channel, "Which brand for second CPU? (Intel or AMD)")

    msg = await client.wait_for_message(author=ctx.message.author)

    if msg.content.lower() == 'amd' :
        await client.send_message(ctx.message.channel, "Please enter search term (i.e. 2700): ")

        msg = await client.wait_for_message(author=ctx.message.author)
        search = msg.content
        matching, cpuList, count = amd_search(search, amd_cpus)

        if len(cpuList) < 1 :
            await client.send_message(ctx.message.channel, "No processors found")
            return
        try :
            await client.send_message(ctx.message.channel, cpuList)
        except :
            await client.send_message(ctx.message.channel, 'Error: List too large. Please refine search.')
            return
        choice = 0
        if count > 1 :
            await client.send_message(ctx.message.channel, 'Which CPU do you want?')
            msg = await client.wait_for_message(author=ctx.message.author)
            try :
                choice = int(msg.content)
                if choice > count or choice < 0 :
                    raise ValueError('Out of Range')
            except :
                await client.send_message(ctx.message.channel, "Error: Not a number or Out of Range. Please restart your search.")
                return

        cpu2 = amd_cpus[matching[choice]]
        cpu2_name = matching[choice]

    elif msg.content.lower() == 'intel' :
        await client.send_message(ctx.message.channel, "Please enter search term (i.e. 8700): ")

        msg = await client.wait_for_message(author=ctx.message.author)
        search = msg.content
        matching, cpuList, count = intel_search(search, intel_cpus)

        if len(cpuList) < 1 :
            await client.send_message(ctx.message.channel, "No processors found")
            return
        try :
            await client.send_message(ctx.message.channel, cpuList)
        except :
            await client.send_message(ctx.message.channel, 'Error: List too large. Please refine search.')
            return
        choice = 0
        if count > 1 :
            await client.send_message(ctx.message.channel, 'Which CPU do you want?')
            msg = await client.wait_for_message(author=ctx.message.author)
            try :
                choice = int(msg.content)
                if choice > count or choice < 0 :
                    raise ValueError('Out of Range')
            except :
                await client.send_message(ctx.message.channel, "Error: Not a number or Out of Range. Please restart your search.")
                return

        cpu2 = intel_cpus[matching[choice]]
        cpu2_name = matching[choice].split('(')[0]
    else :
        await client.send_message(ctx.message.channel, "Unknown brand. Stopping search")
        return
    tbl = compare.compare(cpu1, cpu2, cpu1_name, cpu2_name)

    userOut = '```\n'
    userOut += tbl.table
    userOut += '\n```'

    await client.send_message(ctx.message.channel, userOut)
    #await client.send_message(ctx.message.channel, embed=tbl)

#@client.event
async def on_message(message) :
    global intel_cpus, amd_cpus
    if ctx.message.author == client.user :
        return

    if ctx.message.content.startswith(client.command_prefix + "help") :
        await client.send_message(ctx.message.channel, "$amd to search for AMD CPU\n$intel to search for Intel CPU\n$compare to compare 2 CPUs")

    #if ctx.message.content.startswith(client.command_prefix + "intel") :


    #if ctx.message.content.startswith(client.command_prefix + "amd") :


    #if ctx.message.content.startswith(client.command_prefix + "compare") :


def load_data(intel_cpus, amd_cpus, intel, amd) :
    for i in range(0, len(intel['d'])) :
        intel_cpus[intel['d'][i]['ProductName']] = intel_cpu(
            cores = intel['d'][i]['CoreCount'],
            threads = intel['d'][i]['ThreadCount'],
            base_clock = intel['d'][i]['ClockSpeed'],
            boost_clock = intel['d'][i]['ClockSpeedMax'],
            cache = intel['d'][i]['Cache'],
            tdp = intel['d'][i]['MaxTDP'],
            release_date = intel['d'][i]['BornOnDate'],
            max_memory = intel['d'][i]['MaxMem'],
            lithography = intel['d'][i]['Lithography'],
            socket = intel['d'][i]['SocketsSupported'],
            multiple_cpu = intel['d'][i]['ScalableSockets']
        )
    def try_get_amd_data(data, cpu) :
        info = "N/A"
        try :
            info = str(amd[cpu]['data'][data])
        except :
            pass
        return info
    def try_get_amd_type(cpu) :
        type = ""
        try :
            type = amd[str(cpu)]['type']
        except :
            pass
        return type
    for cpu in amd :
        if try_get_amd_type(cpu) == 'CPU' and amd[str(cpu)]['isPart']:
            amd_cpus[amd[str(cpu)]['humanName']] = amd_cpu(
                cores = str(amd[cpu]['data']['Core Count']),
                threads = str(amd[cpu]['data']['Thread Count']),
                base_clock = str(amd[cpu]['data']['Base Frequency']),
                boost_clock = try_get_amd_data('Base Frequency', cpu),
                cache = try_get_amd_data('L3 Cache (Total)', cpu),
                tdp = str(amd[cpu]['data']['TDP']),
                release_date = try_get_amd_data('Release Date', cpu),
                lithography = str(amd[cpu]['data']['Lithography']),
                socket = str(amd[cpu]['data']['Socket'])
            )

class UpdateWorker(Thread) :
    def __init__(self, queue) :
        Thread.__init__(self)
        self.queue = queue
    def run(self) :
        global intel, amd, intel_cpus, amd_cpus, runTime
        hasRun = 0
        while True :
            cur = datetime.today()
            next = datetime(cur.year, cur.month, cur.day, 3, 0)
            if cur.hour >= 3 :
                next += timedelta(days=1)
            time.sleep((next-cur).seconds)
            update_files(intel, amd, intel_cpus, amd_cpus)

load_data(intel_cpus, amd_cpus, intel, amd)
queue = Queue()
updater = UpdateWorker(queue)
updater.daemon = True
updater.start()
key = open(".bot.key", "r")

client.run(key.read(59))
