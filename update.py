#import ftplib
import requests
import os
import json
from datetime import datetime

def update() :

    #passw = open('.ftp.key','r').read(44)

    #session = ftplib.FTP('ftp.dragonfirecomputing.com','techbot@dragonfirecomputing.com',passw)

    # Intel Update
    intel = json.loads(requests.get('https://odata.intel.com/API/v1_0/Products/Processors()?$format=json').content)
    #wget -O intel.json 'https://odata.intel.com/API/v1_0/Products/Processors()?$format=json'

    # AMD Update
    # INSTALL NPM AND INSIDE SpecDB, RUN npm install js-yaml TO RUN THE BELOW COMMAND
    os.system('cd ../SpecDB; git pull; npm run build && node -e \'require("fs").writeFileSync("/tmp/specs.json", JSON.stringify(require("/tmp/specs.js")))\'')
    amd = json.loads(open('/tmp/specs.json').read())

    #session.storbinary('STOR intel.json', 'intel.json')
    #session.storbinary('STOR specs.json', 'specs.json')
    #session.quit()
    return intel, amd
